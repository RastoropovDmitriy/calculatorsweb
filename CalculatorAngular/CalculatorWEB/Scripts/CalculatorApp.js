﻿var calculator = angular.module("CalculatorModule", []);

calculator.controller("OperatorsCtrl", function ($scope) {

    $scope.digits =
        [
            ["7", "8", "9"],
            ["4", "5", "6"],
            ["1", "2", "3"],
            ["0", "."]
        ];

    $scope.operators =
        [
            ["+", "-"],
            ["*", "/"],
            ["(", ")"]
        ];

    $scope.special =
        [
            ["%", "√x"],
            ["1/x", "+/-"],
            ["M-", "M+"],
            ["MR", "MS"],
            ["MC", "GT"]
        ];

    $scope.expression = "";
    $scope.memory = "";
    $scope.ResultsArr = [];
    $scope.tmp = "";

    // Count expression.
    $scope.Count = function (isRes) {
        if ($scope.expression != eval($scope.expression)) {
            $scope.tmp = $scope.expression;
        }
        $scope.expression = eval($scope.expression);
        if (isRes == undefined & $scope.expression != undefined) {
            $scope.ResultsArr.push($scope.expression);
        }
    };

    // Memory actions.
    $scope.MemoryAct = function (sign) {
        var m = $scope.memory == "" ? 0 : parseFloat($scope.memory);
        var e = $scope.expression == "" ? 0 : parseFloat($scope.expression);
        m = sign == '-' ? m - e : m + e;
        console.log(m);
        $scope.memory = m;
        $scope.expression = "";
    }

    // Grand total.
    $scope.GrandTotal = function () {
        var tmp = 0;
        if ($scope.ResultsArr.length > 1) {
            for (var i = 0; i < $scope.ResultsArr.length; i++) {
                tmp = tmp + $scope.ResultsArr[i];
                console.log(tmp);
            }
            $scope.expression = tmp;
        }
    }

    // Print operator.
    $scope.PrintOperator = function (operator) {
        $scope.expression = $scope.expression + " " + operator + " ";
    }

    // Print digit.
    $scope.PrintDigit = function (digit) {
        $scope.expression = $scope.expression + digit;
    };
    
    // Cansel last action.
    $scope.Cancel = function () {
        $scope.expression = $scope.tmp;
    }

    // Delete last sign.
    $scope.Delete = function () {
        $scope.expression = $scope.expression.slice(0, -1);
    };

    // Clear display.
    $scope.Clear = function () {
        $scope.expression = "";
    };


    $scope.CountSpec = function(symbol) {
        switch (symbol) {
            case "%":
                $scope.Count(0);
                $scope.expression = $scope.expression / 100;
                $scope.ResultsArr.push($scope.expression);
                break;
            case "√x":
                $scope.Count(0);
                if (parseFloat($scope.expression) < 0) {
                    $scope.expression = "error";
                    return;
                }
                $scope.expression = Math.sqrt($scope.expression);
                $scope.ResultsArr.push($scope.expression);
                break;
            case "1/x":
                $scope.Count(0);
                $scope.expression = 1 / $scope.expression;
                $scope.ResultsArr.push($scope.expression);
                break;
            case "+/-":
                $scope.Count(0);
                break;
            case "M-":
                $scope.Count(0);
                $scope.MemoryAct("-");
                break;
            case "M+":
                $scope.Count(0);
                $scope.MemoryAct("+");
                break;
            case "MR":
                $scope.expression = $scope.memory;
                break;
            case "MS":
                $scope.Count(0);
                $scope.memory = $scope.expression;
                $scope.expression = "";
                break;
            case "MC":
                $scope.memory = "";
                break;
            case "GT":
                $scope.GrandTotal();
                break;
            default:
                alert("Oops :)");
                break;
        }
    };

});