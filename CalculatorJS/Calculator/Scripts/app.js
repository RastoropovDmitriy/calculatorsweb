﻿var actionsBin = document.getElementsByClassName('actionBin');
var actionsUn = document.getElementsByClassName('actionUn');
var specActs = document.getElementsByClassName('specAct');
var memActs = document.getElementsByClassName('memory');
var input_numb = document.getElementById('numb');
var exp = document.getElementById('expression');
var exec = document.getElementById('execute');
var result = document.getElementById('result');
var curRes = document.getElementById('res');
var mem = document.getElementById('memory');
var curAct;
var operators = [];

input_numb.addEventListener('blur', function (event) {
    //console.log(this.value);
});


function myBinAction() {

    if (this.value == "=") {
        result.innerHTML = ExecBin(curRes.value, operators.shift(), input_numb.value);
        exp.innerHTML = exp.innerHTML + input_numb.value;
        Clear();
        return;
    }

    if (operators.length == 0) {
        FirstAction(this.value);
        return;
    }

    curAct = this.value;
    operators.push(curAct);
    exp.innerHTML = exp.innerHTML + input_numb.value + curAct;
    curRes.value = ExecBin(curRes.value, operators.shift(), input_numb.value);
    input_numb.value = "";
}

function myUnActions() {

    var action = this.value;
    if (curRes.value == 0){
        result.innerHTML = ExecUn(input_numb.value, action);
        exp.innerHTML = action + input_numb.value;
    }
    else {
        result.innerHTML = ExecUn(curRes.value, action);
        exp.innerHTML = action + curRes.value;
    }
    input_numb.value = "";
    Clear();
}


function spec() {
    var act = this.value;
    console.log(act);
    ExecUn("", act);
}


function myMemAction() {
    var action = this.value;
    ExecUn(input_numb.value, action);
}

// Binary operators
function ExecBin(cvalue, operator, input) {

    switch (operator) {
        case "+":
            return Add(cvalue, input);
            break;
        case "-":
            return Min(cvalue, input);
            break;
        case "/":
            return Div(cvalue, input);
            break;
        case "*":
            return Mult(cvalue, input);
            break;
    }
}

// 
function ExecUn(value, operator) {
    switch (operator) {
        case "√":
            return Math.sqrt(parseFloat(value));
            break;
        case "sin":
            return Math.sin(parseFloat(value));
            break;
        case "cos":
            return Math.cos(parseFloat(value));
            break;
        case "MR":
            MR();
            break;
        case "M+":
            Mplus();
            break;
        case "MC":
            MC();
            break;
        case "MS":
            MS();
            break;
        case "C":
            Clear();
            break;
        case "←":
            Del();
            break;
    }
}

// Actions.
function Add(a, b) {
    return parseFloat(a) + parseFloat(b);
}

function Min(a, b) {
    return parseFloat(a) - parseFloat(b);
}

function Div(a, b) {
    return parseFloat(a) / parseFloat(b);
}

function Mult(a, b) {
    return parseFloat(a) * parseFloat(b);
}

function MS() {
    var input;
    if (curRes == "0")
        input = curRes.value;
    else
        input = input_numb.value;
    mem.value = input;
    input_numb.value = "";
}

function Mplus() {
    var val = mem.value;
    var input = input_numb.value;
    console.log(input);
    if (val != "" & input != "") {
        val = ExecBin(val, "+", input);
    }
    console.log(val);
    mem.value = val;
    input_numb.value = "";
}

function MR() {
    var val = mem.value;
    console.log(val);
    input_numb.value = val;
}

function MC() {
    mem.value = "0";
}


// If action is "="
function Clear() {
    curRes.value = 0;
    input_numb.value = "";
    operators = [];
    console.log(operators);
}

function Del() {
    var input = input_numb.value;
    input = input.slice(0, -1);
    input_numb.value = input;
}

// If first action
function FirstAction(action) {
    curAct = action; 
    operators.push(curAct);
    console.log(operators);
    exp.innerHTML = exp.innerHTML + input_numb.value + curAct;
    curRes.value = input_numb.value;
    input_numb.value = "";
}


function actionBinRetriever(elements) {
    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener("click", myBinAction);
    }
}

function actionUnRetriever(elements) {
    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener("click", myUnActions);
    }
}

function specActRetriever(elements) {
    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener("click", spec);
    }
}

function memoryActRetriever(elements) {
    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener("click", myMemAction);
    }
}

actionBinRetriever(actionsBin);
actionUnRetriever(actionsUn);
specActRetriever(specActs);
memoryActRetriever(memActs);
